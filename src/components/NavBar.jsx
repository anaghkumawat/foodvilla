import { useState } from 'react';
import styles from './NavBar.module.css';
import Logo from '../assets/img/foodvilla.png';
import { Link } from 'react-router-dom';

const NavBar = () => {

    const [isLoggedIn, setIsLoggedIn] = useState(false);

    return (
        <div className={styles.navBarMain}>
            <a href="/">
                <img className={styles.navBarLogo} src={Logo} alt="logo" />
            </a>
            <div className={styles.navBarMenu}>
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/about">About</Link></li>
                    <li><Link to="/contact">Contact</Link></li>
                    <li>Cart</li>
                </ul>
            </div>
            {
                isLoggedIn ? <button onClick={() => setIsLoggedIn(false)}>Logout</button> : <button onClick={() => setIsLoggedIn(true)}>Login</button> 
            }   
        </div>
    );
};

export default NavBar;