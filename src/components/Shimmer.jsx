import styles from './Shimmer.module.css';

const Shimmer = () => {
    return (
        <div className={styles.browseShimmer}>
            {Array(10).fill("").map((e, index) => (<div key={index} className={styles.shimmerCard}></div>))}
        </div>
    );
};

export default Shimmer;