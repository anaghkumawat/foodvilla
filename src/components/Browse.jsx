import RestaurantCard from "./RestaurantCard";
import styles from "./Browse.module.css";
import { useState, useEffect } from "react";
import Shimmer from "./Shimmer";

function filterData(searchInput, restaurants) {
    const filteredData = restaurants.filter((restaurant) => restaurant?.data?.name?.toLowerCase().includes(searchInput.toLowerCase()));
    return filteredData;
}

const Browse = () => {

    const [searchInput, setSearchInput] = useState("");
    const [allRestaurants, setAllRestaurants] = useState([]);
    const [filteredRestaurants, setFilteredRestaurants] = useState([]);

    useEffect(() => {
        getRestaurants();        
    }, []);

    async function getRestaurants() {
        const data = await fetch("https://www.swiggy.com/dapi/restaurants/list/v5?lat=26.9124336&lng=75.7872709&page_type=DESKTOP_WEB_LISTING");
        const json = await data.json();
        setAllRestaurants(json?.data?.cards[2]?.data?.data?.cards);
        setFilteredRestaurants(json?.data?.cards[2]?.data?.data?.cards);
    }

    if (!allRestaurants) return null;

    return (allRestaurants?.length === 0) ? <Shimmer /> : (
        <>
        <div className={styles.searchContainer}>
            <input type="text" className={styles.searchInput} placeholder="Search" value={searchInput}
            onChange={(e) => {
                setSearchInput(e.target.value);
            }}
            />
            <button className={styles.searchBtn} onClick={() => {
                const data = filterData(searchInput,allRestaurants);
                setFilteredRestaurants(data);
            }}
            >Search</button>
        </div>
        <div className={styles.browseRestaurants}>
            {filteredRestaurants.map((restaurant) => {
                return (<RestaurantCard
                    image = {restaurant.data.cloudinaryImageId} 
                    name = {restaurant.data.name}
                    cuisines = {restaurant.data.cuisines}
                    costForTwoString = {restaurant.data.costForTwoString}
                    deliveryTime = {restaurant.data.deliveryTime}
                    avgRating = {restaurant.data.avgRating}
                    key = {restaurant.data.id}
                />

                );
            }
            )}
        </div>
        </>
    );
};

export default Browse;