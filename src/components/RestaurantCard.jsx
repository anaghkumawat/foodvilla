import styles from './RestaurantCard.module.css';
import { IMG_CDN_URL } from '../constants';

const RestaurantCard = (props) => {
    return (
        <div className={styles.restaurantCardMain}>
            <img alt ="logo" src={IMG_CDN_URL+props.image} />
            <h2>{props.name}</h2>
            <h3>{props.cuisines.join(", ")}</h3>
            <div className={styles.restaurantCardOtherDetails}>
                <span>{props.avgRating}</span>
                <div>.</div>
                <span>{props.deliveryTime + " MINS"}</span>
                <div>.</div>
                <span>{props.costForTwoString}</span>   
            </div>
        </div>
    );
};

export default RestaurantCard;