export const IMG_CDN_URL = "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_264,h_288,c_fill/";

export const restaurantList = [
    {

        "type": "restaurant",
        "data": {
            "type": "F",
            "id": "44887",
            "name": "Talk Of The Town",
            "uuid": "58937ca6-87d5-469c-af55-87314ec37064",
            "city": "12",
            "area": "Sindhi Camp",
            "totalRatingsString": "100+ ratings",
            "cloudinaryImageId": "blqlwodl8kqly9hltvzu",
            "cuisines": [
                "Mughlai",
                "North Indian",
                "Biryani",
                "Chinese",
                "Snacks",
                "Afghani",
                "Kebabs",
                "Beverages"
            ],
            "tags": [ ],
            "costForTwo": 45000,
            "costForTwoString": "₹450 FOR TWO",
            "deliveryTime": 27,
            "minDeliveryTime": 25,
            "maxDeliveryTime": 35,
            "slaString": "25-35 MINS",
            "lastMileTravel": 2.0999999046325684,
            "slugs": {
                "restaurant": "talk-of-the-town-mi-road-c-scheme",
                "city": "jaipur"
            },
            "cityState": "12",
            "address": "B-5, Park Street, Khasa Kothi Circle, MI Road, Jaipur",
            "locality": "MI Road",
            "parentId": 4654,
            "unserviceable": false,
            "veg": false,
            "select": false,
            "favorite": false,
            "tradeCampaignHeaders": [ ],
            "aggregatedDiscountInfo": {
                "header": "50% off",
                "shortDescriptionList": [
                    {
                        "meta": "50% off | Use WELCOME50",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "descriptionList": [
                    {
                        "meta": "50% off up to ₹90 | Use code WELCOME50",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
            },
            "aggregatedDiscountInfoV2": {
                "header": "50% OFF",
                "shortDescriptionList": [
                    {
                        "meta": "Use WELCOME50",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "descriptionList": [
                    {
                        "meta": "50% off up to ₹90 | Use code WELCOME50",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
            },
            "ribbon": [
                {
                    "type": "PROMOTED"
                }
            ],
            "chain": [ ],
            "feeDetails": {
                "fees": [ ],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
            },
            "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
            },
            "longDistanceEnabled": 0,
            "rainMode": "NONE",
            "thirdPartyAddress": false,
            "thirdPartyVendor": "",
            "adTrackingID": "cid=5669764~p=1~eid=00000185-bafd-6b2f-450e-adc6000c0111",
            "badges": {
                "imageBased": [ ],
                "textBased": [ ],
                "textExtendedBadges": [ ]
            },
            "lastMileTravelString": "2 kms",
            "hasSurge": false,
            "sla": {
                "restaurantId": "44887",
                "deliveryTime": 27,
                "minDeliveryTime": 25,
                "maxDeliveryTime": 35,
                "lastMileTravel": 2.0999999046325684,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
            },
            "promoted": true,
            "avgRating": "4.2",
            "totalRatings": 100,
            "new": false
        },
        "subtype": "basic"
    
    },
    {
    
        "type": "restaurant",
        "data": {
            "type": "F",
            "id": "249749",
            "name": "McDonald's",
            "uuid": "f020e382-bf63-43e3-80ec-b3dbd81dbacd",
            "city": "12",
            "area": "Bais Godam",
            "totalRatingsString": "1000+ ratings",
            "cloudinaryImageId": "ozyardjn0b2t66ppbfv6",
            "cuisines": [
                "American"
            ],
            "tags": [ ],
            "costForTwo": 40000,
            "costForTwoString": "₹400 FOR TWO",
            "deliveryTime": 32,
            "minDeliveryTime": 32,
            "maxDeliveryTime": 32,
            "slaString": "32 MINS",
            "lastMileTravel": 1.399999976158142,
            "slugs": {
                "restaurant": "mcdonalds-rj-jaipur-crystal-palm-c-scheme",
                "city": "jaipur"
            },
            "cityState": "12",
            "address": "RJ Jaipur Crystal Palm, Plot no-2, Shakar Circle, Sardar Patel Marg,C-scheme, Jaipur - 302004",
            "locality": "RJ Jaipur Crystal Palm",
            "parentId": 630,
            "unserviceable": false,
            "veg": false,
            "select": false,
            "favorite": false,
            "tradeCampaignHeaders": [ ],
            "aggregatedDiscountInfo": {
                "header": "20% off",
                "shortDescriptionList": [
                    {
                        "meta": "20% off | Use TRYNEW",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "descriptionList": [
                    {
                        "meta": "20% off up to ₹50 | Use code TRYNEW",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
            },
            "aggregatedDiscountInfoV2": {
                "header": "20% OFF",
                "shortDescriptionList": [
                    {
                        "meta": "Use TRYNEW",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "descriptionList": [
                    {
                        "meta": "20% off up to ₹50 | Use code TRYNEW",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
            },
            "chain": [ ],
            "feeDetails": {
                "fees": [ ],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
            },
            "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
            },
            "longDistanceEnabled": 0,
            "rainMode": "NONE",
            "thirdPartyAddress": false,
            "thirdPartyVendor": "",
            "adTrackingID": "",
            "badges": {
                "imageBased": [ ],
                "textBased": [ ],
                "textExtendedBadges": [ ]
            },
            "lastMileTravelString": "1.3 kms",
            "hasSurge": false,
            "sla": {
                "restaurantId": "249749",
                "deliveryTime": 32,
                "minDeliveryTime": 32,
                "maxDeliveryTime": 32,
                "lastMileTravel": 1.399999976158142,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
            },
            "promoted": false,
            "avgRating": "4.0",
            "totalRatings": 1000,
            "new": false
        },
        "subtype": "basic"
    
    },
    {
    
        "type": "restaurant",
        "data": {
            "type": "F",
            "id": "108834",
            "name": "Hotel Taj",
            "uuid": "9a98a46b-ede9-45d3-9f28-1dbb4f1a9d58",
            "city": "12",
            "area": "Civil Lines",
            "totalRatingsString": "100+ ratings",
            "cloudinaryImageId": "qmsusgtukm0qzzrrf2vw",
            "cuisines": [
                "North Indian",
                "Thalis",
                "Chinese",
                "Mughlai",
                "Chaat",
                "Punjabi",
                "Desserts",
                "Snacks",
                "Rajasthani",
                "Tandoor",
                "Sweets",
                "Indian",
                "Beverages",
                "Ice Cream",
                "Italian",
                "Pastas"
            ],
            "tags": [ ],
            "costForTwo": 20000,
            "costForTwoString": "₹200 FOR TWO",
            "deliveryTime": 32,
            "minDeliveryTime": 32,
            "maxDeliveryTime": 32,
            "slaString": "32 MINS",
            "lastMileTravel": 1.600000023841858,
            "slugs": {
                "restaurant": "hotel-taj-hasanpura-lal-kothi",
                "city": "jaipur"
            },
            "cityState": "12",
            "address": "SHOP NO. 49. SHRI RAM MARKET, NBC ROAD JAIPUR WARD 13. RAJASTHAN",
            "locality": "Lal Kothi",
            "parentId": 20795,
            "unserviceable": false,
            "veg": false,
            "select": false,
            "favorite": false,
            "tradeCampaignHeaders": [ ],
            "aggregatedDiscountInfo": {
                "header": "60% off",
                "shortDescriptionList": [
                    {
                        "meta": "60% off | Use TRYNEW",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "descriptionList": [
                    {
                        "meta": "60% off up to ₹120 | Use code TRYNEW",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
            },
            "aggregatedDiscountInfoV2": {
                "header": "60% OFF",
                "shortDescriptionList": [
                    {
                        "meta": "Use TRYNEW",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "descriptionList": [
                    {
                        "meta": "60% off up to ₹120 | Use code TRYNEW",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
            },
            "chain": [ ],
            "feeDetails": {
                "fees": [ ],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
            },
            "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
            },
            "longDistanceEnabled": 0,
            "rainMode": "NONE",
            "thirdPartyAddress": false,
            "thirdPartyVendor": "",
            "adTrackingID": "",
            "badges": {
                "imageBased": [ ],
                "textBased": [ ],
                "textExtendedBadges": [ ]
            },
            "lastMileTravelString": "1.6 kms",
            "hasSurge": false,
            "sla": {
                "restaurantId": "108834",
                "deliveryTime": 32,
                "minDeliveryTime": 32,
                "maxDeliveryTime": 32,
                "lastMileTravel": 1.600000023841858,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
            },
            "promoted": false,
            "avgRating": "3.5",
            "totalRatings": 100,
            "new": false
        },
        "subtype": "basic"
    
    },
    {
    
        "type": "restaurant",
        "data": {
            "type": "F",
            "id": "611857",
            "name": "Indian Breads & Curries - IBC",
            "uuid": "df6922c0-0d1e-463d-8488-2ed43de651d0",
            "city": "12",
            "area": "C Scheme",
            "totalRatingsString": "50+ ratings",
            "cloudinaryImageId": "xz1mmyxtbfpmvfcjcdtp",
            "cuisines": [
                "North Indian",
                "Indian"
            ],
            "tags": [ ],
            "costForTwo": 30000,
            "costForTwoString": "₹300 FOR TWO",
            "deliveryTime": 30,
            "minDeliveryTime": 30,
            "maxDeliveryTime": 30,
            "slaString": "30 MINS",
            "lastMileTravel": 1.2999999523162842,
            "slugs": {
                "restaurant": "indian-breads-&-curry---ibc-c-scheme-c-scheme",
                "city": "jaipur"
            },
            "cityState": "12",
            "address": "J-1, JAMNA LAL BAJAJ MARG C-SCHEME JAIPUR,  Jaipur, Jaipur, Rajasthan-302001",
            "locality": "C Scheme",
            "parentId": 365187,
            "unserviceable": false,
            "veg": false,
            "select": false,
            "favorite": false,
            "tradeCampaignHeaders": [ ],
            "aggregatedDiscountInfo": {
                "header": "50% off",
                "shortDescriptionList": [
                    {
                        "meta": "50% off | Use WELCOME50",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "descriptionList": [
                    {
                        "meta": "50% off up to ₹90 | Use code WELCOME50",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
            },
            "aggregatedDiscountInfoV2": {
                "header": "50% OFF",
                "shortDescriptionList": [
                    {
                        "meta": "Use WELCOME50",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "descriptionList": [
                    {
                        "meta": "50% off up to ₹90 | Use code WELCOME50",
                        "discountType": "Percentage",
                        "operationType": "RESTAURANT"
                    }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
            },
            "ribbon": [
                {
                    "type": "PROMOTED"
                }
            ],
            "chain": [ ],
            "feeDetails": {
                "fees": [ ],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
            },
            "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
            },
            "longDistanceEnabled": 0,
            "rainMode": "NONE",
            "thirdPartyAddress": false,
            "thirdPartyVendor": "",
            "adTrackingID": "cid=5668643~p=4~eid=00000185-bafd-6b2f-450e-adc7000c045d",
            "badges": {
                "imageBased": [ ],
                "textBased": [ ],
                "textExtendedBadges": [ ]
            },
            "lastMileTravelString": "1.2 kms",
            "hasSurge": false,
            "sla": {
                "restaurantId": "611857",
                "deliveryTime": 30,
                "minDeliveryTime": 30,
                "maxDeliveryTime": 30,
                "lastMileTravel": 1.2999999523162842,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
            },
            "promoted": true,
            "avgRating": "4.3",
            "totalRatings": 50,
            "new": false
        },
        "subtype": "basic"
    
    }
];